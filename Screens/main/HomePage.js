import React, { Component } from 'react'
import { View, Text, Button, TouchableOpacity, Keyboard, FlatList, list, TextInput, StyleSheet } from 'react-native'
import { MaterialCommunityIcons } from "@expo/vector-icons/"
import StoreMan from "../function/storage";
import Modal from "react-native-modal";
import axios from "axios";
import { API, APIV, Tasks } from "../function/config";

export default class HomePage extends Component {
    constructor(props) {
        super(props);
        this.Storager = new StoreMan();
    }

    state = {
        token: "", data: [], isReady: false,
        fetching_from_server: false, search_text: "",
        endThreshold: 2, text: '', refreshing: true,
        isModalVisible: false,
    }

    componentDidMount = async () => {
        await this.setState({ isReady: false });
        const stored_token = await this.Storager.get_item("USER_TOKEN");
        await this.setState({ token: stored_token });
        await this.Get_Tasks();
        await this.setState({ isReady: true });

    }


    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };


    Get_Tasks = async () => {
        await axios
            .get(API + Tasks, {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "Authorization": " Bearer " + this.state.token
                },
            })
            .then(result => {
                this.setState({
                    data: result.data.data,
                    text: ''
                })
                console.log((this.state.data).length);

            })
            .catch(function (error) {
                console.log(error);
                if (error.response) {
                    //error handling
                    alert(error.response.data.error);
                }
            });

    }

    Post_Task = async () => {
        const token = this.state.token;
        var config = {
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": " Bearer " + token
            }
        };
        var body = {
            "description": this.state.text,

        }
        await axios
            .post(API + Tasks, body, config)
            .then(result => {
                this.toggleModal();
                this.Get_Tasks('reset')
                alert("Task added Successfully")
            })
            .catch(function (error) {
                console.log(error);
                if (error.response) {
                    //error handling
                    alert(error.response.data.error);
                }
            });
    }


    Update_Task = async (taskID, value) => {
        console.log(taskID._id)
        const token = this.state.token;
        let config = {
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": " Bearer " + this.state.token
            }
        };
        let body = {
            "completed": value,

        }
        await axios
            .put(API + Tasks + "/" + taskID._id, body, config)
            .then(result => {
                this.Get_Tasks()
                alert("Task updated Successfully")
            })
            .catch(function (error) {
                console.log(error);
                if (error.response) {
                    //error handling
                    alert(error.response.data.error);
                }
            });
    }



    Delete_Task = async (taskID) => {
        const token = this.state.token;
        var config = {
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": " Bearer " + token
            }
        };
        await axios
            .delete(API + Tasks + "/" + taskID._id, config)
            .then(result => {
                this.Get_Tasks()
                alert("Tasks Deleted Successfully")
            })
            .catch(function (error) {
                console.log(error);
                if (error.response) {
                    //error handling
                    alert(error.response.data.error);
                }
            });
    }


    _onRefresh = () => {
        this.setState({ refreshing: true });
        this.Get_Tasks().then(() => {
            this.setState({ refreshing: false });
        });
    }

    TaskHandler = (value, action) => {

        if (action === "toggle") {
            if (value.completed == true) {
                console.log('trans to false')
                this.Update_Task(value, false)
            } else if (value.completed == false) {
                this.Update_Task(value, true)
            }

        }
        if (action === 'delete') {
            this.Delete_Task(value)
        }
    }



    render() {
        return (
            <View style={{ flex: 1, paddingBottom: 20 }}>
                <Text style={{ alignSelf: 'center' }}>
                </Text>
                <Modal isVisible={this.state.isModalVisible} onBackdropPress={Keyboard.dismiss}>
                    <View style={Pagestyles.modalContainer} >
                        <View style={{ flexDirection: 'row', backgroundColor: 'white', justifyContent: 'space-between', alignContent: 'center', width: 300 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 18 }}>Add a new task</Text>
                            <MaterialCommunityIcons onPress={() => this.toggleModal()} color="#707070" name="close" size={20} />
                        </View>

                        <View style={Pagestyles.modalTextContainer}>
                            <TextInput
                                onChangeText={text => this.setState({ text: text })}
                                style={{ alignSelf: 'center', borderWidth: 0.5, width: 300, paddingHorizontal: 10, borderRadius: 10, borderColor: 'grey' }}
                                value={this.state.text}
                                numberOfLines={3}
                                placeholder="   Name Your task ... "
                            />

                        </View>
                        <View style={{ alignSelf: 'flex-end', marginHorizontal: 15 }}>
                            <Button disabled={this.state.text > 3 ? true : false}
                                title="Done" style={[{
                                    justifyContent: "center", marginHorizontal: 2,
                                    width: 100, borderRadius: 20, height: 40, borderRadius: 1
                                }, this.state.text.length > 3 ? { backgroundColor: '#1DCAFF' } : { backgroundColor: "#ededed" }]} onPress={() => this.Post_Task()} >
                                <Text style={[{}, this.state.text.length > 3 ? { color: 'white' } : { color: "black" }]} >Done</Text>
                            </Button>
                        </View>
                    </View>
                </Modal>
                <TouchableOpacity onPress={() => this.toggleModal()}>
                    <View style={{ width: 250, marginBottom: 20, height: 50, borderRadius: 10, alignSelf: 'center', backgroundColor: '#6495ed', justifyContent: 'center' }}>
                        <Text style={{ alignSelf: 'center' }}>Let's Add a Task </Text>
                    </View>
                </TouchableOpacity>


                {this.state.data.length > 0 ?
                    <FlatList style={{

                    }}
                        scrollEnabled={true}
                        data={this.state.data}
                        ListFooterComponent={<View />}

                        nestedScrollEnabled={true}
                        scrollIndicatorInsets
                        onEndReachedThreshold={this.state.endThreshold}
                        onEndReached={() => {
                            console.log("loadmore")
                            {
                                !this.state.fetching_from_server ?
                                    this.Get_Tasks()
                                    : null
                            }
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index, separators }) => {
                            let value = item;
                            return (
                                < View key={index} data={value} style={[Pagestyles.listContainer, value.completed != true ? { backgroundColor: '#6495ed' } : { backgroundColor: "#3cb371" }]} >
                                    <View style={{ width: 280, height: 50, justifyContent: 'center' }}>
                                        <Text style={{ alignSelf: 'center', color: 'white', fontSize: 17, fontWeight: '500' }}>{value.description} </Text>
                                    </View>
                                    <View style={Pagestyles.actionsContainer}>
                                        <MaterialCommunityIcons
                                            style={{ marginHorizontal: 2, color: value.completed == true ? "#3cb371" : "grey" }} onPress={() => this.TaskHandler(value, "toggle")} color="#707070" name="hand-okay" size={20} />
                                        <MaterialCommunityIcons
                                            style={{ marginHorizontal: 2 }} onPress={() => this.TaskHandler(value, "delete")} color="#707070" name="close" size={20} />
                                    </View>
                                </View>
                            )
                        }
                        }
                    />
                    :
                    <View style={{ alignContent: "center", alignSelf: "center", justifyContent: "center", flex: 1, padding: 20 }}>
                        <Text > There is no data available</Text>
                    </View>}


            </View >
        )
    }
}


const Pagestyles = StyleSheet.create({

    container: { alignSelf: 'center', justifyContent: 'center', flex: 1 },
    modalContainer: { height: 150, backgroundColor: "white", alignItems: "center", justifyContent: "flex-start", padding: 5, borderRadius: 20, flexDirection: 'column' },
    modalTextContainer: {
        flexDirection: "row", flex: 1,
        marginVertical: 5, alignItems: "center", justifyContent: "space-between", paddingHorizontal: 20
    },
    listContainer: { flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between', marginVertical: 5, borderRadius: 10, alignSelf: 'center', borderWidth: 0.5, borderColor: 'grey', marginBottom: 7 },
    actionsContainer: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderTopEndRadius: 10, borderBottomEndRadius: 10, height: 50, paddingEnd: 3, paddingHorizontal: 3, alignSelf: 'flex-end', backgroundColor: 'white' },
    button: { width: 250, height: 30, backgroundColor: '#1E90FF', marginVertical: 5, borderRadius: 9 },
    inputField: { width: 250, height: 35, alignSelf: "center", justifyContent: "space-around", backgroundColor: '#eeeeee', marginVertical: 10, borderWidth: 0.4, borderRadius: 7 },
    textMain: { textAlign: 'center', color: 'white', fontSize: 16 }
})
