import React, { Component } from 'react'
import { View, Text, TextInput, Button, StyleSheet } from 'react-native'
import { API, Login } from "../function/config"
import StoreMan from "../function/storage"
import axios from "axios";
import { StackActions, NavigationActions, CommonActions, } from '@react-navigation/native';

export default class LoginPage extends Component {

    state = {
        isReady: true,
        email: '',
        name: '',
        password: '',
        password_comfirmation: '',
        age: '',
        token: ''
    }

    constructor(props) {
        super(props);
        this.Storager = new StoreMan();
    }

    login = async () => {
        let self = this;
        this.setState({ isReady: false });
        //here you are login request 
        await axios
            .post(API + Login, {
                headers: {
                    "Content-Type": "application/json",

                },
                email: this.state.email,
                password: this.state.password,


            })
            .then(result => {
                console.log("User has been set");
                console.log(result.data.token)
                self.saveData(result.data.token);

            })
            .catch(function (error) {
                if (error.response) {
                    //error handling
                    alert(error.response.data.error);

                }
            });
        this.setState({ isReady: true });
    }


    saveData = (result) => {
        let self = this;
        console.log('save data')
        this.Storager.set_item("USER_TOKEN", result);
        console.log("nav to login")
        this.navigateToLogin();

    }

    navigateToLogin() {
        this.props.navigation.dispatch(
            CommonActions.reset({
                index: 0,
                routes: [{ name: 'Drawer' }]
            })
        )

    }

    render() {
        return (
            this.state.isReady ?
                <View style={Pagestyles.container}>
                    <Text>Create Account
                    </Text>
                    <View style={{ flex: 0.8, alignSelf: 'center', }}>

                        <TextInput
                            style={Pagestyles.inputField}
                            placeholder='Email'
                            textAlign="center"
                            onChangeText={text => this.setState({ email: text })}
                            value={this.state.email}
                        ></TextInput>

                        <TextInput
                            style={Pagestyles.inputField}
                            placeholder='Ente your Password..'
                            secureTextEntry={true}
                            textAlign="center"
                            onChangeText={text => this.setState({ password: text })}
                            value={this.state.password}
                        ></TextInput>



                        <Button onPress={() => { this.login() }} rounded style={Pagestyles.button} title='Log in' >
                        </Button>
                    </View>
                </View>
                : <View>
                    <Text>Loading Screen</Text>
                </View>
        )
    }
}

const Pagestyles = StyleSheet.create({

    container: { alignSelf: 'center', justifyContent: 'center', flex: 1 },
    button: { width: 250, height: 30, backgroundColor: '#1E90FF', marginVertical: 5, borderRadius: 9 },
    inputField: { width: 250, height: 35, alignSelf: "center", justifyContent: "space-around", backgroundColor: '#eeeeee', marginVertical: 10, borderWidth: 0.4, borderRadius: 7 },
    textMain: { textAlign: 'center', color: 'white', fontSize: 16 }
})
