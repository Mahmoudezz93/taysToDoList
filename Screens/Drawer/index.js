import React, { Component } from 'react';
import {View,Text,Textinput} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import StoreMan from "../function/storage";


export default class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.Storager = new StoreMan();
    }

    flush = async () => {
        await this.Storager.remove_all();
        this.props.navigation.replace("Splash")
      }
    render() {
        return (
            <View style={{backgroundColor:'#EEEEEE',flex:1,justifyContent:'center',alignContent:'center',alignItems:'center'}}>
               <TouchableOpacity style={{  alignItems: "center",width:100,height:20,borderRadius:10,borderWidth:0.5,borderColor:"grey" }} onPress={() => this.flush()} >
          <Text>   Logout</Text>
          </TouchableOpacity>
            </View>
        )
    }
}
