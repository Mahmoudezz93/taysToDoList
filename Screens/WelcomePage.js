import React, { Component } from 'react'
import { View, Text, TextInput, Button,StyleSheet } from 'react-native'
import { AsyncStorage } from "react-native"
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class WelcomePage extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <View style={Pagestyles.container}>

                <Text style={{textAlign:'center',marginVertical:10}}>Please login or Signup</Text>
                <TouchableOpacity style={Pagestyles.button}
                    onPress={() => this.props.navigation.navigate('Login')}  ><Text style={Pagestyles.textMain}>Login</Text></TouchableOpacity>

                <TouchableOpacity
                style={Pagestyles.button}
                    onPress={() => this.props.navigation.navigate('Signup')} title=' Create Account' >
                        <Text style={Pagestyles.textMain}>Signup</Text>
                </TouchableOpacity>
        </View>
        )
    }
}

const Pagestyles = StyleSheet.create({

container : { alignSelf:'center',justifyContent:'center',flex:1},
button:{width:250,height:30,backgroundColor:'#1E90FF',marginVertical:5,borderRadius:9},

textMain:{textAlign:'center',color:'white',fontSize:16}
})
