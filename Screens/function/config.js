export const API = "https://api-nodejs-todolist.herokuapp.com/"

// Sign up // 
export const Signup = "user/register"; 
export const Login = "user/login"; 

// Tasks // 
export const Tasks = "task"
