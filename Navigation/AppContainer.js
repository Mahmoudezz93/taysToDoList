import React, {Component} from "react";
import { NavigationContainer } from "@react-navigation/native"; 
import {createDrawerNavigator} from "@react-navigation/drawer"
import { createStackNavigator } from "@react-navigation/stack";

// import screens // 

// Splash Screen // 
import Splash from "../Screens/Splash"
import WelcomePage from "../Screens/WelcomePage";

// Auth Stack // 
import Login from "../Screens/Auth/Login";
import SignUp from "../Screens/Auth/SignUp";

// Drawer // 
import Sidebar from "../Screens/Drawer";

// Main Stack // 
import HomePage from "../Screens/main/HomePage";
import Profile from "../Screens/main/MyProfile";


const MainStackNavigator = createStackNavigator();
export const MainNavigator = ()=>
{
    return(
        <MainStackNavigator.Navigator initialRouteName="Home"screenOptions={{ headerShown: false }} >
            <MainStackNavigator.Screen name="Home" component={HomePage} />
            <MainStackNavigator.Screen name="Profile" component={Profile} />

        </MainStackNavigator.Navigator>
    )
}


const DrawerStackNavigator = createDrawerNavigator();
export const DrawerNavigator = ()=>
{
    return (
        <DrawerStackNavigator.Navigator
         screenOptions={{  activeTintColor: "#e91e63" ,headerShown: false}}
        drawerPosition={"right"}
        drawerContent={(props) => <Sidebar {...props} />}>
            <DrawerStackNavigator.Screen  name="Main" component={MainNavigator}/>
        </DrawerStackNavigator.Navigator>
    )
}


const LoginStack = createStackNavigator();
/* This is the main container for the application an should contain all other stacks for the application 
*/ 

export default AppContainer = props =>
{
    return(
        <NavigationContainer>
            <LoginStack.Navigator HeaderMode="none"  initialRouteName="Splash">
                <LoginStack.Screen name="Welcome" component={WelcomePage}/>
                <LoginStack.Screen name="Splash" title="Welcome" component={Splash}/>
                <LoginStack.Screen name="Login" component={Login}/>
                <LoginStack.Screen name="Signup" component={SignUp}/>
                <LoginStack.Screen name="Drawer" component={DrawerNavigator}/>
            </LoginStack.Navigator>
        </NavigationContainer>
    );
};